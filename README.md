# JWT Authentication in Lumen Project

## What is JWT ?
JWT stands for JSON Web Token. JSON Web Token (JWT) is an open standard (RFC 7519) that defines a compact and self-contained way for securely transmitting information between parties as a JSON object.


## Why we use JWT ?
- **Authorization:** This is the most common scenario for using JWT. Once the user is logged in, each subsequent request will include the JWT, allowing the user to access routes, services, and resources that are permitted with that token. Single Sign On is a feature that widely uses JWT nowadays, because of its small overhead and its ability to be easily used across different domains.

- **Information Exchange:** JSON Web Tokens are a good way of securely transmitting information between parties. Because JWTs can be signed — for example, using public/private key pairs — you can be sure the senders are who they say they are. Additionally, as the signature is calculated using the header and the payload, you can also verify that the content hasn't been tampered with.


## What is the JSON Web Token structure?
In its compact form, JSON Web Tokens consist of three parts separated by dots (.), which are:
- Header <br/> 
- Payload <br/> 
- Signature <br/> 

Therefore, a JWT typically looks like the following.

xxxxx.yyyyy.zzzzz

Let's break down the different parts.

# Jwt configure in Lumen project

## What is Lumen?

Lumen is the perfect solution for building Laravel based micro-services and blazing fast APIs. In fact, it's one of the fastest micro-frameworks available. It has never been easier to write stunningly fast services to support your Laravel applications.


## Why use Lumen
It is only micro service which is call  1900 times requests per second.

## What is API

API is the stand for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other. Each time you use an app like Facebook, send an instant message, or check the weather on your phone, you’re using an API.

## Prerequisite

Make sure you have the essentials, I beg of you.

- PHP >= 7.1.3 <br/> 
- OpenSSL PHP Extension <br/> 
- PDO PHP Extension <br/> 
- Mbstring PHP Extension <br/> 
- Mysql >= 5.7 <br/> 
- Composer (Dependency Manager for PHP)> <br/> 
- Postman (To test your endpoints) <br/> 

## Installation

First, download the Lumen installer using Composer:<br/> 

        command: composer global require "laravel/lumen-installer"

<br/>then you select your directory for lumen project and give the below command in your terminal <br/> 

        command: lumen new auth-app

Here "auth-app" is your project name you can set this as your wish.
now enter the project folder with <br/> 

        command: cd auth-app 

now you need to run the app <br/> 

        command: php -S localhost:8000 -t public 

now go to browser and type localhost:8000 and run , now see your lumen project interfce.
Open up the project (auth-app) in your preferred editor.

## Application Key

At first you should edit the .env.example file to .env file. and 
Now you need to set a 32 characters  key for ***APP_KEY*** and also set ***JWT_SECRET*** for 32 characters.<br/>
***like:*** <br/> 

      APP_KEY=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8

      JWT_SECRET=JhbGciOiJIUzI1N0eXAiOiJKV1QiLC

      this app key secure your project.

## Uncomment function:

In boostrap/app.php uncomment the facades and eloquent method

    //before

    // $app->withFacades();

    // $app->withEloquent();

    //after

    $app->withFacades();

    $app->withEloquent();

## Create a user Table:

    php artisan make:migration create_users_table 

Locate the migration file database/migrations/*_create_users_table.php and add neede table columns(name, email, password); see code below:


        <?php

        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;

        class CreateUsersTable extends Migration
        {
  
            public function up()
            {
                Schema::create('users', function (Blueprint $table) {

                        $table->bigIncrements('id');
                        $table->string('name');
                        $table->string('email')->unique()->notNullable();
                        $table->string('password');
                        $table->timestamps();
                });
            }

            /**
            * Reverse the migrations.
            *
            * @return void
            */
            public function down()
            {
                Schema::dropIfExists('users');
            }
        }

Now migrate your databse. with the command

    php artisan migrate

## Route file setting:
go to your project and open web.php in routes/web.php and copy paste below code

        <?php


        $router->group(['prefix' => 'api'], function () use ($router) {
            // Matches "/api/register
        $router->post('register', 'AuthController@register');
            // Matches "/api/login
            $router->post('login', 'AuthController@login');

            // Matches "/api/profile
            $router->get('profile', 'UserController@profile');

            // Matches "/api/users/1
            //get one user by id
            $router->get('users/{id}', 'UserController@singleUser');

            // Matches "/api/users
            $router->get('users', 'UserController@allUsers');
        });

## Controller file setting:
create a controller file named AuthController.php in app/Http/Controllers/AuthController.php 


        <?php
        namespace App\Http\Controllers;
        use Illuminate\Http\Request;
        use App\User;
        use Illuminate\Support\Facades\Auth;

        class AuthController extends Controller
        {



            public function login(Request $request)
            {
                //validate incoming request
                $this->validate($request, [
                    'email' => 'required|string',
                    'password' => 'required|string',
                ]);

                $credentials = $request->only(['email', 'password']);

                if (! $token = Auth::attempt($credentials)) {
                    return response()->json(['message' => 'Unauthorized'], 401);
                }

                return $this->respondWithToken($token);
            }



            /**
            * Store a new user.
            *
            * @param  Request  $request
            * @return Response
            */
            public function register(Request $request)
            {
                //validate incoming request
                $this->validate($request, [
                    'name' => 'required|string',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|confirmed',
                ]);

                try {

                    $user = new User;
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $plainPassword = $request->input('password');
                    $user->password = app('hash')->make($plainPassword);

                    $user->save();

                    //return successful response
                    return response()->json(['user' => $user, 'message' => 'CREATED'], 201);

                } catch (\Exception $e) {
                    //return error message
                    return response()->json(['message' => 'User Registration Failed!'], 409);
                }

            }


        }


and now create another controller file named ***UserController.php***
and copy paste below code.


        <?php

        namespace App\Http\Controllers;

        use Illuminate\Support\Facades\Auth;
        use  App\User;

        class UserController extends Controller
        {
            /**
            * Instantiate a new UserController instance.
            *
            * @return void
            */
            public function __construct()
            {
                $this->middleware('auth');
            }

            /**
            * Get the authenticated User.
            *
            * @return Response
            */
            public function profile()
            {
                return response()->json(['user' => Auth::user()], 200);
            }

            /**
            * Get all User.
            *
            * @return Response
            */
            public function allUsers()
            {
                return response()->json(['users' =>  User::all()], 200);
            }

            /**
            * Get one user.
            *
            * @return Response
            */
            public function singleUser($id)
            {
                try {
                    $user = User::findOrFail($id);

                    return response()->json(['user' => $user], 200);

                } catch (\Exception $e) {

                    return response()->json(['message' => 'user not found!'], 404);
                }

            }

        }


***Change in controller file***  copy and paste this code in Controller.php


    <?php

    namespace App\Http\Controllers;
    use Illuminate\Support\Facades\Auth;
    use Laravel\Lumen\Routing\Controller as BaseController;

    class Controller extends BaseController
    {
        protected function respondWithToken($token)
        {
            return response()->json([
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => Auth::factory()->getTTL() * 60
            ], 200);
        }
    }



## User sign in:
Pull in the JWT authentication package.

        composer require tymon/jwt-auth:dev-develop


## Create auth file
In root directory create a folder named config. then create a file in config folder named auth.php


        //config.auth.php

        <?php

        return [
            'defaults' => [
                'guard' => 'api',
                'passwords' => 'users',
            ],

            'guards' => [
                'api' => [
                    'driver' => 'jwt',
                    'provider' => 'users',
                ],
            ],

            'providers' => [
                'users' => [
                    'driver' => 'eloquent',
                    'model' => \App\User::class
                ]
            ]
        ];

## Revised user model
go to User.php in app/User.php and copy paste below code


        <?php
        namespace App;

        use Illuminate\Auth\Authenticatable;
        use Laravel\Lumen\Auth\Authorizable;
        use Illuminate\Database\Eloquent\Model;
        use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
        use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


        use Tymon\JWTAuth\Contracts\JWTSubject;

        class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
        {
            use Authenticatable, Authorizable;

            /**
            * The attributes that are mass assignable.
            *
            * @var array
            */
            protected $fillable = [
                'name', 'email','password',
            ];

            /**
            * The attributes excluded from the model's JSON form.
            *
            * @var array
            */
            protected $hidden = [
                'password',
            ];




            /**
            * Get the identifier that will be stored in the subject claim of the JWT.
            *
            * @return mixed
            */
            public function getJWTIdentifier()
            {
                return $this->getKey();
            }

            /**
            * Return a key value array, containing any custom claims to be added to the JWT.
            *
            * @return array
            */
            public function getJWTCustomClaims()
            {
                return [];
            }
        }

## Revise app.php
in bootstrap folder app.php make some changes in below types


        //before
        // $app->routeMiddleware([
        //     'auth' => App\Http\Middleware\Authenticate::class,
        // ]);

        //After
        $app->routeMiddleware([
            'auth' => App\Http\Middleware\Authenticate::class,
        ]);
        //before
        // $app->register(App\Providers\AppServiceProvider::class);
        // $app->register(App\Providers\AuthServiceProvider::class);
        // $app->register(App\Providers\EventServiceProvider::class);

        //After
        // $app->register(App\Providers\AppServiceProvider::class);
        $app->register(App\Providers\AuthServiceProvider::class);
        // $app->register(App\Providers\EventServiceProvider::class);

        // Add this line
        $app->register(Tymon\JWTAuth\Providers\LumenServiceProvider::class);


## Set method:
We have to insure our route file method and postman method is same. like when we use this localhost:8000/api/register route and set the method is post.

## Register a user:
Register a user(use POSTMAN) with route localhost:8000/api/register and you should get a successful response like so
![postmanRegister](/uploads/f4ad9b746ef2dad4e88efde3f66e806b/postmanRegister.jpg)


## Login as a user:
Login a user using route localhost:8000/api/login and you should get a successful response like so:
![postmanRegister](/uploads/f4ad9b746ef2dad4e88efde3f66e806b/postmanRegister.jpg)![postmanLogin](/uploads/de20055884802708ca22af8cdf851fd0/postmanLogin.jpg)

## Token Use: 

### Unauthorized access:
here create a token. now we use this token for further login and see details like profile. if we do not use token then we have ***unathorized*** access.Now we go to access for user profile by link:
 ***localhost:8000/api/profile***  but we give back unathorized.

![unathorized](/uploads/fd47565bebe4fcd5da1ad87938047d00/unathorized.jpg)


### Authorized access:

ow we use this token in headers with key: authorization and value: bearer with token
now if we see the profile then we see this 
![authorized](/uploads/3e89fd9353272d57cd2287e2da638fd7/authorized.jpg)

and with same token we also see the users with link:
 ***localhost:8000/api/users*** 
